from django.conf import settings
from django.urls import path
from .views import DecodeAccountInfo
from drf_spectacular.views import SpectacularAPIView, SpectacularRedocView, SpectacularSwaggerView

urlpatterns = [
    path('decode-acc-info/', DecodeAccountInfo.as_view(), name='decode-acc-info')
]

# These urls are for api documentation
# Add them only when django DEBUG mode in on
if settings.DEBUG is True:
    urlpatterns += [
        # For downloading schema yaml
        path('schema/', SpectacularAPIView.as_view(), name='schema'),
        # Optional UI:
        path('schema/swagger-ui/', SpectacularSwaggerView.as_view(url_name='schema'), name='swagger-ui'),
        path('schema/redoc/', SpectacularRedocView.as_view(url_name='schema'), name='redoc'),
    ]
