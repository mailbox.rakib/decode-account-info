from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase


class DecodeAccountInfoTests(APITestCase):
    def test_decoded_account_info_200(self):
        """
        Ensure that the decoded account info is correct
        """
        url = reverse('decode-acc-info')
        request_data = {
            "requestId": "A32W4ER2341",
            "accountName": "TXIuIEFCQw==",
            "amount": "aSN2QHZYeExjRE0h"
        }
        expected_response_data = {
            "requestId": "A32W4ER2341",
            "accountName": "Mr. ABC",
            "amount": 1681
        }
        response = self.client.post(url, request_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(isinstance(response.data['amount'], int), 'Amount returned should be a integer.')
        self.assertEqual(expected_response_data, response.data)

    def test_decoded_account_info_400(self):
        """
        Ensure that the incorrect base64 string is detected
        """
        url = reverse('decode-acc-info')
        request_data = {
            "requestId": "A32W4ER2341",
            "accountName": "XIuIEFCQw=",
            "amount": "SN2QHZYeExjRE0"
        }

        response = self.client.post(url, request_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['accountName'][0]), 'Invalid Base64 encoded string')
        self.assertEqual(str(response.data['amount'][0]), 'Invalid Base64 encoded string')
