import logging
from rest_framework import serializers
from .validators import Base64Validator
import base64
from functools import reduce
logger = logging.getLogger('account_info')
ROMAN_CHAR_MAPPING = {
    'I': 1,
    'V': 5,
    'X': 10,
    'L': 50,
    'C': 100,
    'D': 500,
    'M': 1000
}


class AccountInfoDecodingSerializer(serializers.Serializer):
    """
    Sample Request:
    {
        “requestId”: “A32W4ER2341”,
        “accountName”: “TXIuIEFCQw==”
        “amount”: “aSN2QHZYeExjRE0h”
    }
        Request fields are described below:
        “requestId” - String and unique value
        “accountName” - Base64 encoded String represents account holder’s name
        “amount” - Base64 encoded String, contains an amount as account balance.

    1. Decode (Base64) the accountName from request and print it (eg. Mr. ABC)
    2. Decode (Base64) the amount from request and print it (eg. i#v@vXxLcDM!)
    3. Remove all special characters (from the result of step 2) and keep only the
        alphabets. Print the resulting string (eg. ivvXxLcDM).
    4. Capitalize all characters of the string (found in step 3) and print it (eg. IVVXXLCDM)
    5. From the output of step 4, you will find a string containing only the following roman
        characters: 'M', 'D', 'C', 'L', 'X', 'V', 'I'. Where each roman character represents
        following decimal values:
            'I' = 1
            'V' = 5
            'X' = 10
            'L' = 50
            'C' = 100
            'D' = 500
            'M' = 1000
        For each character, sum up every representing decimal value and print the final
        amount value. (eg: 1681)
    Sample Response:
    {

        "requestId": "A32W4ER2341",
        "accountName": "Mr. ABC",
        "amount": 1681
    }
    """

    requestId = serializers.CharField()
    accountName = serializers.CharField(validators=[Base64Validator()])
    amount = serializers.CharField(validators=[Base64Validator()])

    def validate_accountName(self, value):
        base64_decoded = base64.b64decode(value).decode('utf-8')
        logger.info(f'Base64 decoded `account name` is {base64_decoded}')
        return base64_decoded

    def validate_amount(self, value):
        base64_decoded = base64.b64decode(value).decode('utf-8')
        logger.info(f'Base64 decoded `amount` is {base64_decoded}')

        minus_special_char = ''.join(filter(str.isalnum, base64_decoded))
        logger.info(f'After removing special chars other than alphanumeric `amount` is {minus_special_char}')

        all_upper = minus_special_char.upper()
        logger.info(f'After converting all chars to uppercase `amount` is {all_upper}')

        # Add a leading 0 to apply `reduce` function effectively
        amount = reduce(lambda x, y: int(x) + ROMAN_CHAR_MAPPING.get(y, 0), '0'+all_upper)
        # logger.info(f'Finally the `amount` is {amount}')

        return amount
