from drf_yasg import openapi
from drf_spectacular.utils import extend_schema
from .serializers import AccountInfoDecodingSerializer

# This the schema dict for the decoded account info response.
account_info_response_fields = {
    "type": openapi.TYPE_OBJECT,
    "title": "Decoded Account Amount",
    "properties": {
        "requestId": openapi.Schema(
            title="Requested ID getting from post data",
            type=openapi.TYPE_STRING,
        ),
        "accountName": openapi.Schema(
            title="Base64 decoded Account Name",
            type=openapi.TYPE_STRING,
        ),
        "amount": openapi.Schema(
            title="Decoded amount",
            type=openapi.TYPE_INTEGER,
        ),
    },
    "required": ["requestId", "accountName", "amount"],
}


def decode_account_info_swagger():
    """
    This method is used to return the decorator used for showing the OpenApi 3 standard documentation
    :return: decorator `extend_schema` with some initialized params for `DecodeAccountInfo` APIView
    """
    return extend_schema(
        request=AccountInfoDecodingSerializer,
        responses={200: account_info_response_fields},
        summary='Decoding the `accountName` and `amount` field as Base64 string. '
                'Sum the roman numbers inside `amount` to get the final result.'
    )
