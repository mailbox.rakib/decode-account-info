

class conditional_decorator(object):
    def __init__(self, dec, condition):
        self.decorator = dec
        self.condition = condition

    def __call__(self, func):
        if self.condition is True:
            return self.decorator(func)
        # Return the function unchanged, not decorated.
        return func
