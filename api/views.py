from django.conf import settings
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import AccountInfoDecodingSerializer
from .helpers import conditional_decorator
from .swagger_schemas import decode_account_info_swagger


class DecodeAccountInfo(APIView):
    permission_classes = []
    allowed_methods = ['POST']

    @conditional_decorator(decode_account_info_swagger(), settings.DEBUG)
    def post(self, request, *args, **kwargs):
        serializer = AccountInfoDecodingSerializer(data=request.data)
        if serializer.is_valid():
            # For converting the serialized data `amount` from string to int first copy them to another dict
            # If that conversion is not needed we can just `return Response(serializer.data, status=200)`
            response = serializer.data
            response['amount'] = int(response['amount'])
            return Response(response, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
