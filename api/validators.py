import re
from rest_framework import serializers
pattern = re.compile("^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$")


class Base64Validator:
    def __call__(self, value):
        if not pattern.match(value):
            message = 'Invalid Base64 encoded string'
            raise serializers.ValidationError(message)


