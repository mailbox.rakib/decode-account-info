from django.http import HttpResponseNotFound


def custom_page_not_found_view(request, exception):
    return HttpResponseNotFound('<h1>Page not found</h1>')
