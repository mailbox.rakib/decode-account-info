from django.urls import path, include

handler404 = 'account_service.views.custom_page_not_found_view'
urlpatterns = [
    path('api/', include('api.urls')),
]
