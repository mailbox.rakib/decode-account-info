# base image
FROM python:3.7.5
# setup environment variable
ENV DockerHOME=/home/app/decode_account_info

# set work directory
RUN mkdir -p $DockerHOME

# where the actual code lives
WORKDIR $DockerHOME

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install dependencies
RUN pip install --upgrade pip

# copy whole project to your docker home directory.
COPY . $DockerHOME
# run this command to install all dependencies
RUN pip install -r requirements.txt

# set the os environment variable `DEBUG`
# enable DEBUG
# ENV DEBUG=TRUE
# enable production
ENV DEBUG=FALSE

# port where the Django app runs
EXPOSE 8000
# start django development server
#CMD python manage.py runserver 0.0.0.0:8000

# start gunicorn server with 4 worker thread and setting the log level to DEBUG
CMD gunicorn --bind 0.0.0.0:8000 --workers 4 --log-level DEBUG account_service.wsgi