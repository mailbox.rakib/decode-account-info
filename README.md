# Account Information Decoding Microservice 

## Requirements
* Python 3.7.5 
* Django==3.2
* djangorestframework==3.13.1
* drf-spectacular==0.23.1
* drf-yasg==1.21.3 \
`drf-spectacular` and `drf-yasg` are used for generating OpenApi 3 standard documentation. \
Note that documentation is ony avialable in `DEBUG` mode. Not in production.

## Run Commands for Local Machine
* Assuming you have correct python version installed (A virtual environment is recomended). 
* Please execute `pip install -r requirements.txt` from project root when running for the first time. 
* To run the django app execute `python manage.py runserver 0.0.0.0:8000` from project root which run the project on localhost 8000 port. 

## Endpoints & Request Response Format
> 'api/decode-acc-info/' receive post data in this sample format 
```json
{
    "requestId": "Test",
    "accountName": "TXIuIEFCQw==",
    "amount": "aSN2QHZYeExjRE0h"
}
```
Request fields are described below: 
* “requestId” - String and unique value 
* “accountName” - Base64 encoded String represents account holder’s name 
* “amount” - Base64 encoded String, contains an amount as account balance.

The response for the above request is (with status code 200): 
```json
{
  "requestId": "Test",
  "accountName": "Mr. ABC",
  "amount": 1681
}
``` 

## Docker Compose Instructions 
To run the dockerized container of the microservice you can use docker-compose commands: 
* `docker-compose up -d` build a conatiner named `drf_microservice_account_info` and run it on `8090` port in local macnhine
* So you can use the `localhost:8090/api/decode-acc-info/` from now on for the post operation
* To stop the docker container you can run `docker-compose down -v` \
**_`gunicorn` is used as wsgi server and `DBUG` value is set to `FALSE` for the micoservice while running inside dockerized container._** \
**_To run the service with django `DEBUG` mode on please set the environment variable `DEBUG=TRUE` in `Dockerfile`._** 


## Documentaion URLs (_Only available when django `DEBUG` mode is on._)

* For swagger `api/schema/swagger-ui/`
* For redoc `api/schema/redoc/`
* For download schema YAML `api/schema/`

## Tests
Two test case is included to test:
1. For a valid request response returned is correct or not
2. Invalid request is handeled properly \
To run the test cases execute the command `python manage.py test` from project root
